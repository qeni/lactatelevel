#!/usr/bin/env python2


import os
import cv2
import time
import argparse
import picamera
import RPi.GPIO as GPIO

out_pin_first = 14
out_pin_second = 15
out_pin_third = 18

# to use Raspberry Pi BCM pin numbers
GPIO.setmode(GPIO.BCM)

# setup leds on board
GPIO.setup(out_pin_first, GPIO.OUT)
GPIO.setup(out_pin_second, GPIO.OUT)
GPIO.setup(out_pin_third, GPIO.OUT)

# disable warnings
GPIO.setwarnings(False)

# Parsing arguments
parser = argparse.ArgumentParser()

parser.add_argument("-d", "--dest", help="set destination directory for the movies",
                    action='store', type=str, default="/home/pi/.lactatelevel/movies/")

parser.add_argument("-pd", "--pdest", help="set destination directory for the photos",
                    action='store', type=str, default="/home/pi/.lactatelevel/photos/")

parser.add_argument("-fd", "--fdest", help="set destination directory for the frames",
                    action='store', type=str, default="/home/pi/.lactatelevel/frames/")

parser.add_argument("-n", "--name", help="name of the output movie",
                    action='store', type=str, default="test.h264")

parser.add_argument("-p", "--pname", help="name of the output photo",
                    action='store', type=str, default="img.png")

parser.add_argument("--mono", action='store_true', default=False, help="only black and white images")

parser.add_argument("-f", "--fps", help="set frames per second",
                    type=int, default=90)

parser.add_argument("-m", "--mode", help="mode of operation",
                    type=int, default=2)

parser.add_argument("-t", "--time", help="time of recording in seconds",
                    type=int, default=20)

parser.add_argument("-x", "--xres", help="width",
                    type=int, default=640)

parser.add_argument("-y", "--yres", help="height",
                    type=int, default=480)

parser.add_argument("-r", "--rotation", help="rotation in degrees",
                    type=int, default=180)

parser.add_argument("-l", "--led", help="enable leds",
                    type=int, default=3)

parser.add_argument("-v", "--verbose", action="store_true",
                    help="increase output verbosity")

args = parser.parse_args()


class LactateCamera:

    def __init__(self):
        self.camera = picamera.PiCamera()
        self.camera.led = False
        self.camera.resolution = (args.xres, args.yres)
        self.camera.rotation = args.rotation
        self.camera.framerate = args.fps
        self.camera.awb_mode = 'off'
        self.camera.awb_gains = (0.9,0.9)

        if args.mono:
            # turn camera to black and white
            self.camera.color_effects = (128,128)

        # camera warm-up time
        if args.verbose:
            print "Starting the camera..."
        time.sleep(2)

    def take_photo(self):
        if args.verbose:
            print "Taking a photo..."
        self.camera.capture(args.pdest + args.pname)

    def take_ten_photos(self):
        if args.verbose:
            print "Taking 10 pictures..."
        for i in range(10):
            self.camera.capture(args.pdest + 'img_' + str(i).zfill(3) + '.png')

    def start_movie(self):
        if args.verbose:
            print "Recording..."
        self.camera.start_recording(args.dest + args.name)
        self.camera.wait_recording(args.time)
        self.camera.stop_recording()

    def split_frames(self):
        vidcap = cv2.VideoCapture(args.dest + args.name)
        success,image = vidcap.read()
        count = 0
        success = True

        while success:
            success,image = vidcap.read()
            if args.verbose:
                print 'Read a new frame: ', success
            cv2.imwrite(args.fdest + "frame_{0:05}.png".format(count), image)
            count += 1

    def enable_leds(self):
        if args.verbose:
            print "Enabling leds..."
        if args.led == 1:
            GPIO.output(out_pin_first, GPIO.HIGH)
        elif args.led == 2:
            GPIO.output(out_pin_first, GPIO.HIGH)
            GPIO.output(out_pin_second, GPIO.HIGH)
        elif args.led == 3:
            GPIO.output(out_pin_first, GPIO.HIGH)
            GPIO.output(out_pin_second, GPIO.HIGH)
            GPIO.output(out_pin_third, GPIO.HIGH)

    def disable_leds(self):
        if args.verbose:
            print "Disabling leds..."
        GPIO.output(out_pin_first, GPIO.LOW)
        GPIO.output(out_pin_second, GPIO.LOW)
        GPIO.output(out_pin_third, GPIO.LOW)


def main():
    camera = LactateCamera()
    camera.enable_leds()

    if args.mode == 0:
        camera.take_photo()
    elif args.mode == 1:
        camera.take_ten_photos()
    elif args.mode == 2:
        camera.start_movie()
    elif args.mode == 3:
        camera.split_frames()
        # os.system("gimp -ifd -b '(batch-auto-levels '~/.lactatelevel/frames/*.png')' -b '(gimp-quit 0)'")

    elif args.mode == 4:
        img = cv2.imread('~/.lactatelevel/frames/frame_00000.png')        
        retval, threshold = cv2.threshold(img, 12, 255, cv2.THRESH_BINARY)
        grayscaled = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
        retval2, threshold = cv2.threshold(grayscaled, 12, 255, cv2.THRESH_BINARY)
        cv2.imwrite("frame_00000.png".format(count), image)

    camera.disable_leds()


if __name__ == "__main__":
    main()
