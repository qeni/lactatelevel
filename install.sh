#!/usr/bin/env bash

sudo apt update
sudo apt install python python-pip gimp python-scipy python-opencv -y

mkdir -p ~/.lactatelevel/movies
mkdir -p ~/.lactatelevel/photos
mkdir -p ~/.lactatelevel/frames
mkdir -p ~/.gimp-2.8/scripts/

cp batch-auto-levels.scm ~/.gimp-2.8/scripts
