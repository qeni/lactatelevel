![N|Solid](https://img.shields.io/badge/python-2.7-blue.svg) ![N|Solid](https://img.shields.io/badge/raspberry%20pi-zero%20w-ce1818.svg)
# Measuring blood lactate level with Raspberry Pi

![N|Solid](img/powered_by.png)

---
## Hardware requirements:
- Raspberry Pi Zero W
- Camera + FFC cable for RPi Zero
- TODO: what else

---
## Installation
```bash
git clone https://qeni@gitlab.com/qeni/lactatelevel.git
cd lactatelevel
./install.sh
virtualenv <location> --system-site-packages
source <location>/bin/activate
pip install -r requirements.txt
```

---
## Usage
usage: lactatelevel.py [-h] [-d DEST] [-pd PDEST] [-n NAME] [-p PNAME] [-f FPS] [-m MODE] [-t TIME] [-x XRES] [-y YRES] [-r ROTATION] [-l NUMBER] [-v] [--mono]

optional arguments:  
  -h, --help  
  show this help message and exit

  -d DEST, --dest DEST  
  set destination directory for the movies

  -pd PDEST, --pdest PDEST  
  set destination directory for the photos

  -n NAME, --name NAME  
  name of the output movie

  -p PNAME, --pname PNAME  
  name of the output photo

  -f FPS, --fps FPS  
  set frames per second

  -m MODE, --mode MODE  
  mode of operation (0 - take photo, 1 - take 10 photos, 2 - start a movie,
  3 - split frames)

  -t TIME, --time TIME  
  time of recording in seconds

  -x XRES, --xres XRES  
  width

  -y YRES, --yres YRES  
  height

  -r ROTATION, --rotation ROTATION  
  rotation in degrees

  --mono
  turn camera to black and white 

  -l, --led             
  enable leds

  -v, --verbose         
  increase output verbosity

---
## License
TODO: choose a license
