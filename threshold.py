#!/usr/bin/env python2

import os
import cv2
from scipy import ndimage

for i in range(1800):
    img = cv2.imread('/home/qeni/.lactatelevel/frames/frame_{0:05}.png'.format(i))        
    print(ndimage.measurements.center_of_mass(img))
    grayscaled = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    retval2, threshold2 = cv2.threshold(grayscaled, 95, 255, cv2.THRESH_BINARY)
    cv2.imwrite("/home/qeni/.lactatelevel/frames/frame_{0:05}.png".format(i), threshold2)
