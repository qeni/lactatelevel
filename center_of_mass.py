#!/usr/bin/env python2

import os
import cv2
import pylab
import numpy as np
import math
import matplotlib.pyplot as plt
from scipy import ndimage, arange
from mpl_toolkits.mplot3d import Axes3D
import scipy.fftpack
from scipy.signal import blackmanharris
import common
import random

size = 1800

x = np.arange(0,size)
y = []
z = []

for i in range(size):
    # img = cv2.imread('/home/qeni/.lactatelevel/frames/frame_{0:05}.png'.format(i)) 
    img = cv2.imread('/home/qeni/.lactatelevel/frames_autolevel/frame_{0:05}.png'.format(i)) 
    # img = cv2.imread('/home/qeni/.lactatelevel/frames_original/frame_{0:05}.png'.format(i)) 
    # img = cv2.imread('/home/qeni/tmp/frames_threshold_przed/frame_{0:05}.png'.format(i)) 
    # img = cv2.imread('/home/qeni/tmp/frames_threshold_po/frame_{0:05}.png'.format(i)) 
    y.append(ndimage.measurements.center_of_mass(img)[0])
    z.append(ndimage.measurements.center_of_mass(img)[1])

    print("Frame: {} -- > {}/{}".format(i, y[-1], z[-1]))

y_average = np.mean(y)
z_average = np.mean(z)

print("\n\n\n")

for i in range(size):
    a = pow(y[i]-y_average, 2)
    b = pow(z[i]-z_average, 2)

distance  = []
for i in range(size):
    distance.append(math.sqrt(y[i]**2 + z[i]**2))

with open('new.txt', 'r+') as f:
    for i in distance:
        f.write(str(i).format('%f') + ' ')

def plotSpectrum(y,Fs):
    """
    Plots a Single-Sided Amplitude Spectrum of y(t)
    """
    n = len(y) # length of the signal
    k = arange(n)
    T = n/Fs
    frq = k/T # two sides frequency range
    frq = frq[range(n/2)] # one side frequency range

    Y = scipy.fftpack.fft(y)/n # fft computing and normalization
    # for i in Y:
    #     print(abs(i))
    Y = Y[range(n/2)]

    plt.plot(frq,abs(Y),'r') # plotting the spectrum
    plt.xlabel('Freq (Hz)')
    plt.ylabel('|Y(freq)|')
    plt.show()

Fs = 1.0;  # sampling rate

distance = scipy.signal.medfilt(distance, 11)
plotSpectrum(distance[100:],Fs)

# plt.plot(x, distance)
# plt.plot(x[10:], fft[10:])
# plt.show()
